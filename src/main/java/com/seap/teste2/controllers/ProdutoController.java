package com.seap.teste2.controllers;

import com.seap.teste2.models.Produtos;
import com.seap.teste2.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value="/api")
public class ProdutoController {

    @Autowired
    ProdutoRepository produtoRepository;

    @GetMapping("/produtos")
    public List<Produtos> listarProdutos(){
        return produtoRepository.findAll();
    }

    @PostMapping("/produto")
    public Produtos adicionaProduto(@RequestBody Produtos produto){
        return produtoRepository.save(produto);
    }

    @DeleteMapping("/produto/{id}")
    public void deletaProduto(@PathVariable(value="id") long id){
        produtoRepository.deleteById(id);
    }

    @PutMapping("/produto")
    public Produtos alteraProduto(@RequestBody Produtos produto){
        return produtoRepository.save(produto);
    }

    @GetMapping("produto/{id}")
    public Produtos listaUnicoProduto(@PathVariable(value = "id") long id){
        return produtoRepository.findById(id);
    }
}
