package com.seap.teste2.controllers;

import com.seap.teste2.models.Pessoas;
import com.seap.teste2.repositories.PessoasRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class PessoasController {

    @Autowired
    private PessoasRepository pessoasRepository;

   @GetMapping("/pessoas")
    public List<Pessoas> listaTodas(){
       return pessoasRepository.findAll();
   }

   @GetMapping("pessoas/{id}")
   public Pessoas listaUnicaPessoa(@PathVariable(value = "id") long id){
       return pessoasRepository.findById(id);
   }

   @PostMapping("/pessoa")
    public  Pessoas salvarPessoa(@RequestBody Pessoas pessoas){
       return pessoasRepository.save(pessoas);
   }

    @DeleteMapping("/pessoa/{id}")
    public void deletePessoa(@PathVariable(value = "id")long id){
         pessoasRepository.deleteById(id);
    }

    @PutMapping("/pessoa")
       public Pessoas updatePessoa(@RequestBody Pessoas pessoas){
        return pessoasRepository.save(pessoas);
    }

}