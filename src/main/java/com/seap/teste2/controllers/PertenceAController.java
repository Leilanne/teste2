package com.seap.teste2.controllers;

import com.seap.teste2.models.PertenceA;
import com.seap.teste2.repositories.PertenceARepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class PertenceAController {

    @Autowired
    PertenceARepository pertenceARepository;

    @GetMapping("/pertences")
    public List<PertenceA> listaTodas(){
       return pertenceARepository.findAll();
    }

    @PostMapping("/pertence")
    public PertenceA AssociaPertence(@RequestBody PertenceA pertenceA){
        return pertenceARepository.save(pertenceA);
    }

    @DeleteMapping("/pertence/{id}")
    public void deletaPertence(@PathVariable(value = "id") long id){
        pertenceARepository.deleteById(id);
    }

    @PutMapping("/pertence")
    public PertenceA updatePertence(@RequestBody PertenceA pertenceA){
        return pertenceARepository.save(pertenceA);
    }
}
