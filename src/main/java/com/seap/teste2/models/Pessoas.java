package com.seap.teste2.models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="pessoas")
public class Pessoas implements Serializable {

    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    @Column(name="id")
    private long id;

    @Column(name="nome")
    private String nome;

    @Column(name="rg")
    private Long rg;

    public Long getPessoa_id() {
        return id;
    }

    public void setPessoa_id(Long pessoa_id) {
        this.id = pessoa_id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Long getRg() {
        return rg;
    }

    public void setRg(Long rg) {
        this.rg = rg;
    }
}
