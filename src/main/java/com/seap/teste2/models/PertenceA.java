package com.seap.teste2.models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="PertenceA")
public class PertenceA implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private long id;

    @ManyToOne
    @JoinColumn(name = "id_pessoa")
    private Pessoas pessoa;

    @ManyToOne
    @JoinColumn(name = "id_produto")
    private Produtos produto;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Pessoas getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoas pessoa) {
        this.pessoa = pessoa;
    }

    public Produtos getProduto() {
        return produto;
    }

    public void setProduto(Produtos produto) {
        this.produto = produto;
    }
}
