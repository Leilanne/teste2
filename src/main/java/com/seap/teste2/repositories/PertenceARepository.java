package com.seap.teste2.repositories;

import com.seap.teste2.models.PertenceA;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PertenceARepository extends JpaRepository<PertenceA, Long> {

    PertenceA findById(long id);
}
