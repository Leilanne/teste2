package com.seap.teste2.repositories;

import com.seap.teste2.models.Produtos;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProdutoRepository extends JpaRepository<Produtos, Long> {

    Produtos findById(long id);
}
