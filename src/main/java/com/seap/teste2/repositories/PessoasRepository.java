package com.seap.teste2.repositories;

import com.seap.teste2.models.Pessoas;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PessoasRepository extends JpaRepository<Pessoas,Long> {

    Pessoas findById(long id);
}
